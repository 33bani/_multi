<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
table.type10 {
	border-spacing: 1px;
	text-align: center;
	line-height: 1.5;
	margin: 20px 10px;
}

table.type10 th {
	width: 155px;
	padding: 10px;
	font-weight: bold;
	vertical-align: center;
	color: #fff;
	background: #e0daf1;
}

table.type10 td {
	width: 155px;
	padding: 10px;
	vertical-align: center;
	border-bottom: 1px solid #ccc;
	background: #ffffff;
}
</style>
</head>
<body>
	<div id="header">
		<jsp:include page="header.jsp"></jsp:include>
	</div>
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>주문 관리</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->
	<!-- service_details_start  -->
	<div class="service_details_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="service_details_left">
						<h3>관리자 모드</h3>
						<div class="nav nav-pills" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">
							<a class="" id="v-pills-profile-tab" href="/TeamProject/poption"
								role="tab" aria-controls="v-pills-profile" aria-selected="false">상품
								관리 </a> <a class="" id="v-pills-profile-tab"
								href="/TeamProject/moption" role="tab"
								aria-controls="v-pills-profile" aria-selected="false">고객 관리</a>

							<a class=" active" id="v-pills-home-tab" data-toggle="pill"
								href="v-pills-profile" role="tab" aria-controls="v-pills-home"
								aria-selected="true">주문 관리</a>

						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home"
							role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="service_details_info">
								<table class="type10">
									<tr>
										<th>주문번호</th>
										<th>아이디</th>
										<th>상품코드</th>
										<th>상품명</th>
										<th>수령인</th>
										<th>수령인 번호</th>
										<th>주소</th>
										<th>개수</th>
										<th>가격</th>
										<th>주문일자</th>
									</tr>
									<c:forEach var="olist" items="${olist}">
										<tr>
											<td>${olist.oid}</td>
											<td>${olist.porder.ocid}</td>
											<td>${olist.opcode}</td>
											<td>상품명</td>
											<td>${olist.porder.orec}</td>
											<td>${olist.porder.ophone}</td>
											<td>${olist.porder.oaddress}</td>
											<td>${olist.oquantity}</td>
											<td>${olist.porder.oprice}</td>
											<td>${olist.porder.odate}</td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>