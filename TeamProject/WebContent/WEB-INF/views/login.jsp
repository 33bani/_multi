<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>SIGN IN</title>

</head>
<body>
	<!-- header-start -->
	<div id="header">
		<jsp:include page="header.jsp" />
	</div>
	<!-- header-end -->

	<!-- Estimate_area start  -->
	<div class="Estimate_area overlay">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-5">
					<div class="Estimate_info">
						<h3>로그인</h3>
						<p>
							<c:if test="${msg == false}">
								<a style="color: red">존재하지 않는 아이디 또는 비밀번호 입니다.</a>
							</c:if>
						</p>
					</div>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-7">
					<div class="form">


						<form action='<c:url value='/loginOK'/>' method="post">
							<div class="row">
								<div class="col-xl-12">
									<div class="input_field">
										<p style="color: white">ID</p>
										<input type="text" name="id" placeholder="아이디 또는 이메일을 입력해주세요.">
									</div>
								</div>
								<div class="col-xl-12">
									<div class="input_field">
										<p style="color: white">PASSWORD</p>
										<input type="password" name="password"
											placeholder="비밀번호를 입력해주세요.">
									</div>
								</div>
								<c:if test="${member == null}">
									<div class="col-xl-6">
										<div class="input_field">
											<button class="boxed-btn3-line" type="submit">로그인</button>
										</div>
									</div>
								</c:if>
								<br>
								<div class="col-xl-6">
									<div class="input_field">
										<input class="boxed-btn3-line" type="button" value="회원가입" onclick="location.href='/TeamProject/signup';"/>

									</div>
								</div>
								<div class="col-xl-12" style="text-align: center">
									<div class="input_field">
										<br> <a style="color: white" href="#">아이디 찾기</a> | <a
											style="color: white" href="#">비밀번호 찾기</a>
									</div>
								</div>
							</div>
						</form>

						<c:if test="${member != null}">
							<c:redirect url="/main" />
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Estimate_area end  -->


	<!-- footer -->
	<div id="footer">
		<jsp:include page="footer.jsp" />
	</div>
	<!-- /footer -->
</body>
</html>