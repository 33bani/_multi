<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>HOME</title>

<body>
	<!-- header -->
	<div id="header">
		<jsp:include page="header.jsp"/>
	</div>
	<!-- /header -->

	<!-- slider_area_start style="background-image: url('/img/about/0ffcan.png');"-->
	<div class="contact_action_area" style="background-image: url('../img/dog.png');">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-7 col-md-6">
					<div class="action_heading">
						<h3>Brand Story</h3>
						<p>이유있는 선택, 정전기를 부탁해</p>
					</div>
				</div>
				<div class="col-xl-5 col-md-6">
					<div class="call_add_action">
						<a href="/TeamProject/productList/dog" class="boxed-btn3">강아지 용품 보러 바로가기</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /contact_action_area  -->


	<!-- best -->
	<div class="service_area">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="section_title mb-50 text-center">
						<h3>BEST 5</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-12">
					<div class="service_active owl-carousel">
						<c:forEach var="best" items="${best}">
							<div class="single_service">
								<div class="thumb">
									<img src="<spring:url value='/images/${best.pimg}'/>">
								</div>
								<div class="service_info"
									onclick="location.href='/TeamProject/productDetail/${best.pcode}'">
								<h3>
									<a href="/TeamProject/productDetail/${best.pcode}">[${best.pbrand}]
										${best.pname}</a>
								</h3>
								<p>
									<fmt:formatNumber value="${best.pprice}" pattern="###,###"/>원
								</p>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /best end-->
	<!-- slider_area_start -->
	<div class="contact_action_area">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-xl-8">
					<div class="slider_text text-center justify-content-center">
						<p style="color: white">놓치기 아까운 이달의 상품</p>
						<h3 style="color: white">EVENT</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- slider_area_end -->

	<!-- footer -->
	<div id="footer">
		<jsp:include page="footer.jsp"/>
	</div>
	<!-- /footer -->

</body>
</html>




