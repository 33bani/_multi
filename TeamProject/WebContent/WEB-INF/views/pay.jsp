<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div id="header">
		<jsp:include page="header.jsp"></jsp:include>
	</div>
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center" style="color: white">
						<h3>결제 완료되었습니다.</h3><br><br><br>
						<span onMouseover="this.style.color='pink';"
							onMouseout="this.style.color='white';"
							onclick="location.href='/TeamProject/main';"> 홈페이지로 이동 </span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="footer">
		<jsp:include page="footer.jsp"></jsp:include>
	</div>
</body>
</html>