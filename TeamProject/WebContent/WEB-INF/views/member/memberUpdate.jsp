<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>정보 변경</title>
<style>
table.type10 {
	border-spacing: 1px;
	text-align: center;
	line-height: 1.5;
	margin: 20px 10px;
}

table.type10 th {
	width: 155px;
	padding: 10px;
	font-weight: bold;
	vertical-align: center;
	color: #fff;
	background: #e0daf1;
}

table.type10 td {
	width: 155px;
	padding: 10px;
	font-size: 12px;
	vertical-align: center;
	border-bottom: 1px solid #ccc;
	background: #ffffff;
}
</style>
</head>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	function fn_pwCheck() {
		$.ajax({
			url : "/TeamProject/pwCheck",
			type : "post",
			dataType : "json",
			data : {
				"id" : $("#id").val(),
				"password" : $("#password").val()
			},
			success : function(data) {
				if (data == 1) {
					$("#pwCheck").attr("value", "비밀번호 일치");
					$(".msg").text("비밀번호가 일치합니다.");
					$(".msg").attr("style", "color:#00f");
					$("#password").attr("readonly", "readonly");
				} else if ($("#password").val() == "") {
					$(".msg").text("비밀번호를 입력하세요.");
					$(".msg").attr("style", "color:#f00");
				} else if (data == 0) {
					$(".msg").text("비밀번호가 맞지 않습니다.");
					$(".msg").attr("style", "color:#f00");
				}
			}
		})
	}
	$(document).ready(function() {
		$("#submit").on("click", function() {
			var pwCheckVal = $("#pwCheck").val();
			if (pwCheckVal == "정보 확인") {
				alert("정보 확인 버튼을 눌러주세요.");
				return false;
			}
			if ($("#password1").val() == "") {
				alert("비밀번호를 입력해주세요.");
				$("#pwd1").focus();
				return false;
			}
			if ($("#phone").val() == "") {
				alert("휴대폰번호를 입력해주세요.");
				$("#phone").focus();
				return false;
			}
			if ($("#email").val() == "") {
				alert("이메일을 입력해주세요.");
				$("#email").focus();
				return false;
			}
			alert("수정이 완료되었습니다.");
		});
	})
</script>
<body>
	<!-- header -->
	<div id="header">
		<jsp:include page="../header.jsp" />
	</div>
	<!-- /header -->
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>회원 정보 수정</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->
	<!-- service_details_start  -->
	<div class="service_details_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="service_details_left">
						<h3>회원 정보</h3>
						<div class="nav nav-pills" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">
							<a class="" id="v-pills-profile-tab"
								href="/TeamProject/memberInfo" role="tab"
								aria-controls="v-pills-profile" aria-selected="false">기본 정보
							</a> <a class="active" id="v-pills-home-tab"
								href="/TeamProject/memberUpdate" role="tab" data-toggle="pill"
								aria-controls="v-pills-homev-pills-profile" aria-selected="true">회원
								정보 수정 </a> <a class="" id="v-pills-profile-tab"
								href="/TeamProject/memberDelete" role="tab"
								aria-controls="v-pills-homev-pills-profile"
								aria-selected="false">회원 탈퇴</a>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home"
							role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="service_details_info">
								<h3>정보 확인</h3>
								<div onload="info()">
									<form method="post" action="mUpdate">
										<!-- 회원정보를 가져와 member 변수에 담는다. -->
										<c:set var="member" value="${member}" />

										<!-- 입력한 값을 전송하기 위해 form 태그를 사용한다 -->
										<!-- 값(파라미터) 전송은 POST 방식 -->


										<table class="type10">
											<tr>
												<td id="title">아이디</td>
												<td><input type="text" name="id" id="id"
													value="${member.id}" readonly="readonly"></td>
											</tr>
											<tr> <td id ="title">비밀번호</td>
												<td><input type="password" name="password"
													id="password" maxlength="50"></td>
												<td style="border-bottom: none"><input type="button"
													id="pwCheck" onclick="fn_pwCheck();" class="genric-btn success-border radius" value="정보 확인"></td>
											</tr>
											<tr>
												<td style="color:#dfdfdf;">정보 확인</td>
												<td><span class="msg"></span></td>
											</tr>
										</table>
										<br>
										<h3>정보 수정</h3>
										<br>
										<table class="type10">
											<tr>
												<td id="title">비밀번호</td>
												<td><input type="password" name="password1"
													id="password1" maxlength="50" value="${member.password}"></td>
											</tr>
											<tr>
												<td id="title">이름</td>
												<td>${member.name}</td>
											</tr>
											<tr>
												<td id="title">성별</td>
												<td>${member.gender}</td>
											</tr>

											<tr>
												<td id="title">생일</td>
												<td>${member.birthday}</td>
											</tr>

											<tr>
												<td id="title">이메일</td>
												<td>${member.email}</td>
											</tr>

											<tr>
												<td id="title">휴대전화</td>
												<td><input type="text" name="phone" id="phone"
													value="${member.phone}" /></td>
											</tr>
											<tr>
												<td id="title">주소</td>
												<td><input type="text" name="address" id="address"
													value="${member.address}" /></td>
											</tr>
										</table>
										<br> <br>
										<div class="button-group-area mt-40">
											<button id="submit" class="genric-btn info radius"
												type="submit">수정</button>
											<button class="genric-btn info radius"
												onclick="location.href='/TeamProject/main'">취소</button>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- footer -->
	<div id="footer">
		<jsp:include page="../footer.jsp" />
	</div>
	<!-- /footer -->
</body>
</html>