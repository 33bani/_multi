<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib  prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>장바구니</title>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp"></jsp:include>
	</div>
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
				
					<div class="bradcam_text text-center">
						<h3>장바구니</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="sample-text-area">
		<div class="container box_1170">
			<div class="section-top-border">
				<div>
					<div class="progress-table">
						<div class="table-head">
							<div class="serial">#</div>
							<div class="visit">상품명</div>
							<div class="visit">상품이미지</div>
							<div class="visit">수량</div>
							<div class="visit">가격</div>
							<div class="visit">총금액</div>
							<div class="visit"></div>
						</div>
						<%int j=1; %>
						<c:forEach var="cartList" items="${cartList}">
							<div class="table-row" id="no${cartList.ccode}">
								<div class="serial"><%=j++%></div>
								<div class="visit">
									${cartList.pname}
								</div>
								<div class="visit">
								<a href="/TeamProject/productDetail/${cartList.pcode}"><img width="70" src= "<spring:url value='/images/${cartList.pimg}'/>"></a>
								</div>
								<div class="visit">
									${cartList.cquantity}
								</div>
								<div class="visit">
									<fmt:formatNumber pattern="#,###.##" value= "${cartList.cprice/cartList.cquantity}" />원
								</div>
								<div class="visit">
									<fmt:formatNumber pattern="#,###.##" value= "${cartList.cprice}" />원
								</div>
								<div class="visit">
								 <button class="genric-btn info-border radius" onclick="location.href='cartList/del/${cartList.pcode }'">삭제</button>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				<div class="button-group-area mt-40">
					<button class="genric-btn info radius"
						onclick="location.href='/TeamProject/checkOrder'">주문하기</button>
					<button class="genric-btn info radius" onclick="location.href='/TeamProject/main'">메인페이지로</button>
				</div>
			</div>
		</div>
		<div id="footer">
			<jsp:include page="../footer.jsp"></jsp:include>
		</div>
</body>
</html>