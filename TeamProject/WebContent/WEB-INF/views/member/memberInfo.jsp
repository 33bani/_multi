<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>회원정보</title>

<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
	<script type="text/javascript" >
</script>
</head>
<body>
	<!-- header-start -->
	<div id="header">
		<jsp:include page="../header.jsp" />
	</div>
	<!-- header-end -->
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>회원 정보</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->
	<!-- service_details_start  -->
	<div class="service_details_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="service_details_left">
						<h3>회원 정보</h3>
						<div class="nav nav-pills" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">

							<a class="active" id="v-pills-home-tab" data-toggle="pill"
								href="/TeamProject/memberInfo" role="tab"
								aria-controls="v-pills-profile" aria-selected="true">기본 정보 </a>

							<a class="" id="v-pills-profile-tab"
								href="/TeamProject/memberUpdate" role="tab"
								aria-controls="v-pills-homev-pills-profile"
								aria-selected="false">회원 정보 수정 </a> <a class=""
								id="v-pills-profile-tab" href="/TeamProject/memberDelete"
								role="tab" aria-controls="v-pills-homev-pills-profile"
								aria-selected="false">회원 탈퇴</a>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home"
							role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="service_details_info">

								<!-- 회원정보를 가져와 member 변수에 담는다. -->
								<c:set var="member" value="${member}" />

								<!-- 가져온 회원정보를 출력한다. -->
								<div class="col-md-6 mt-sm-30">
									<h3 class="mb-20">${member.name }님의 정보</h3>
									<div class="">
										<ul class="unordered-list">
											<img src="img/profile.png" width="200" height="200">
											<li>아이디 : ${member.id}</li>
											<li>이름 : ${member.name}</li>
											<li>성별 : ${member.gender}</li>
											<li>생년 월일 : ${member.birthday}</li>
											<li>이메일 : ${member.email}</li>
											<li>휴대전화 : ${member.phone}</li>
											<li>주소 : ${member.address}</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /service -->
	<!-- footer -->
	<div id="footer">
		<jsp:include page="../footer.jsp" />
	</div>
	<!-- /footer -->
</body>
</html>