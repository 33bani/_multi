<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>회원가입</title>
</head>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	function fn_idCheck() {
		$.ajax({
			url : "/TeamProject/idCheck",
			type : "post",
			dataType : "json",
			data : {
				"id" : $("#id").val()
			},
			success : function(data) {
				if (data == 1) {
					$(".msg").text("중복된 아이디입니다.");
					$(".msg").attr("style", "color:#f00");
				} else if ($("#id").val() == "") {
					$(".msg").text("아이디를 입력하세요.");
					$(".msg").attr("style", "color:#f00");
				} else if (data == 0) {
					$("#idCheck").attr("value", "중복확인 완료");
					$(".msg").text("사용가능한 아이디입니다.");
					$(".msg").attr("style", "color:#00f");
					$("#id").attr("readonly", "readonly");
				}
			}
		})
	}

	$(function() {
		$("#alert-success").hide();
		$("#alert-danger").hide();
		$("input").keyup(function() {
			var pwd1 = $("#pwd1").val();
			var pwd2 = $("#pwd2").val();
			if (pwd1 != "" || pwd2 != "") {

				if (pwd1 == pwd2) {
					$("#alert-success").show();
					$("#alert-danger").hide();
				} else {
					$("#alert-success").hide();
					$("#alert-danger").show();
				}
			}
		});
	});

	$(document).ready(
			function() {
				$("#submit").on(
						"click",
						function() {
							if ($("#id").val() == "") {
								alert("아이디를 입력해주세요.");
								$("#id").focus();
								return false;
							}
							if ($("#pwd1").val() == "") {
								alert("비밀번호를 입력해주세요.");
								$("#pwd1").focus();
								return false;
							}
							if ($("#name").val() == "") {
								alert("이름을 입력해주세요.");
								$("#name").focus();
								return false;
							}
							if ($("#birthyear").val() == ""
									|| $("#birthmonth").val() == ""
									|| $("#birthdate").val() == "") {
								alert("생년월일을 입력해주세요.");
								$("#birthyear").focus();
								return false;
							}
							if ($("#phone2").val() == ""
									|| $("#phone3").val() == "") {
								alert("휴대폰번호를 입력해주세요.");
								$("#phone2").focus();
								return false;
							}
							if ($("#email").val() == "") {
								alert("이메일을 입력해주세요.");
								$("#email").focus();
								return false;
							}
							if ($("#address").val() == "") {
								alert("주소를 입력해주세요.");
								$("#address").focus();
								return false;
							}
							var idCheckVal = $("#idCheck").val();
							if (idCheckVal == "중복확인") {
								alert("중복확인 버튼을 눌러주세요.");
								return false;
							}
							var pwd1 = $("#pwd1").val();
							var pwd2 = $("#pwd2").val();
							if (pwd1 != "" || pwd2 != "") {
								if (pwd1 !== pwd2) {
									alert("비밀번호를 확인해주세요.");
									$("#pwd1").val();
									return false;
								}
							}
							alert("회원가입이 완료되었습니다.");
						});
			})
</script>
<body>
	<!-- header-start -->
	<div id="header">
		<jsp:include page="header.jsp" />
	</div>
	<!-- header-end -->

	<div class="Estimate_area overlay">
		<div class="container">
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-md-5">
					<div class="Estimate_info">
						<h3>회원 가입</h3>
						<p>회원가입을 위해 정보를 입력해 주세요.</p>
					</div>
				</div>
				<div class="col-xl-8 col-lg-8 col-md-7">
					<div class="form">
						<form action="add" method="post">
							<div class="row">
								<div class="col-xl-5">
									<div class="input_field">
										<input type="text" id="id" name="id" placeholder="아이디">
									</div>
								</div>
								<div class="col-xl-3">
									<div class="input_field">
										<input type="button" id="idCheck" onclick="fn_idCheck();"
											value="중복확인">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="input_field">
										<span class="msg"></span>
									</div>
								</div>
								<div class="col-xl-4">
									<div class="input_field">
										<input type="password" id="pwd1" name="password"
											placeholder="비밀번호">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="input_field">
										<input type="password" id="pwd2" placeholder="비밀번호 재확인">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="input_field">
										<div id="alert-success">
											<font color=blue>비밀번호가 일치합니다.</font>
										</div>
										<div id="alert-danger">
											<font color=red>비밀번호가 일치하지 않습니다.</font>
										</div>
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<input type="text" id="name" name="name" placeholder="이름">
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<select class="wide" name="gender">
											<option value="남성">남성</option>
											<option value="여성">여성</option>
										</select>
									</div>
								</div>
								<div class="col-xl-3">
									<div class="input_field">
										<select class="wide" id="birthyear" name="birthyear">
											<option data-display="출생년도" value="">출생년도</option>
											<%
												for (int i = 2021; i >= 1950; i--) {
											%>
											<option value="<%=i%>"><%=i + "년"%></option>
											<%
												}
											%>
										</select>
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<select class="wide" name="birthmonth" id="birthmonth">
											<option data-display="월" value="">월</option>
											<%
												for (int i = 1; i <= 12; i++) {
											%>
											<option value="<%=i%>"><%=i + "월"%></option>
											<%
												}
											%>
										</select>
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<select class="wide" name="birthdate" id="birthdate">
											<option data-display="일" value="">일</option>
											<%
												for (int i = 1; i <= 31; i++) {
											%>
											<option value="<%=i%>"><%=i + "일"%></option>
											<%
												}
											%>
										</select>
									</div>
								</div>

								<div class="col-xl-6">
									<div class="input_field">
										<input type="email" name="email" id="email" placeholder="이메일">
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<select class="wide" name="phone1">
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="016">016</option>
											<option value="017">017</option>
											<option value="018">018</option>
											<option value="019">019</option>
										</select>
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<input type="text"
											onKeyup="this.value=this.value.replace(/[^0-9]/g,'');"
											name="phone2" maxlength='4' id="phone2" placeholder="앞번호">
									</div>
								</div>
								<div class="col-xl-2">
									<div class="input_field">
										<input type="text"
											onKeyup="this.value=this.value.replace(/[^0-9]/g,'');"
											name="phone3" maxlength='4' id="phone3" placeholder="뒷번호">
									</div>
								</div>
								<div class="col-xl-12">
									<div class="input_field">
										<textarea name="address" id="address" placeholder="주소"></textarea>
									</div>
								</div>
								<div class="col-xl-8">
									<div class="input_field">
										<button class="boxed-btn3-line" type="submit" id="submit">가입하기</button>
									</div>
								</div>
								<div class="col-xl-4">
									<div class="input_field">
										<input type="button"
											onclick="location.href='/TeamProject/main'" value="취소">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer -->
	<div id="footer">
		<jsp:include page="footer.jsp" />
	</div>
	<!-- /footer -->
</body>
</html>