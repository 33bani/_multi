<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>상품 관리</title>
<style>
table.type10 {
	border-spacing: 1px;
	text-align: center;
	line-height: 1.5;
	margin: 20px 10px;
}

table.type10 th {
	width: 155px;
	padding: 10px;
	font-weight: bold;
	vertical-align: center;
	color: #fff;
	background: #e0daf1;
}

table.type10 td {
	width: 155px;
	padding: 10px;
	font-size: 12px;
	vertical-align: center;
	border-bottom: 1px solid #ccc;
	background: #ffffff;
}
</style>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#submit").on("click", function() {
			if ($("#pname").val() == "") {
				alert("상품이름을 입력해주세요.");
				$("#pname").focus();
				return false;
			}
			if ($("#pbrand").val() == "") {
				alert("브랜드명을 입력해주세요.");
				$("#pbrand").focus();
				return false;
			}
			if ($("#pprice").val() == "") {
				alert("가격을 입력해주세요.");
				$("#pprice").focus();
				return false;
			}
			alert("상품등록에 성공하였습니다.");
		});
	})
</script>
<body>
	<!-- header-start -->
	<div id="header">
		<jsp:include page="../header.jsp" />
	</div>
	</header>
	<!-- header-end -->

	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>상품 관리</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->

	<!-- service_details_start  -->
	<div class="service_details_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="service_details_left">
						<h3>관리자 모드</h3>
						<div class="nav nav-pills" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">
							<a class=" active" id="v-pills-home-tab" data-toggle="pill"
								href="v-pills-profile" role="tab" aria-controls="v-pills-home"
								aria-selected="true">상품 관리 </a> <a class=""
								id="v-pills-profile-tab" href="/TeamProject/moption" role="tab"
								aria-controls="v-pills-profile" aria-selected="false">고객 관리
							</a><a class="" id="v-pills-profile-tab" href="/TeamProject/ooption"
								role="tab" aria-controls="v-pills-profile" aria-selected="false">주문
								관리 </a>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home"
							role="tabpanel" aria-labelledby="v-pills-home-tab">

							<div class="accordion_area">
								<div class="faq_ask">
									<h3>상품 관리</h3>
									<div id="accordion">
										<div class="card">
											<div class="card-header" id="headingTwo">
												<h5 class="mb-0">
													<button class="btn btn-link collapsed"
														data-toggle="collapse" data-target="#collapseTwo"
														aria-expanded="false" aria-controls="collapseTwo">
														<span>상품 등록</span>
													</button>
												</h5>
											</div>
											<div id="collapseTwo" class="collapse"
												aria-labelledby="headingTwo" data-parent="#accordion"
												style="">
												<div class="card-body">
													<form action="addp" method="post"
														enctype="multipart/form-data">
														<table class="type10">
															<tr>
																<td>상품 종류 :</td>
																<td><input type="radio" name="code" value="10000"
																	checked>강아지 <input type="radio" name="code"
																	value="20000">고양이</td>
															</tr>
															<tr>
																<td>상품 이름:</td>
																<td><input type="text" name="pname" id="pname"></td>
															</tr>
															<tr>
																<td>브랜드명:</td>
																<td><input type="text" name="pbrand" id="pbrand"></td>
															</tr>
															<tr>
																<td>상세 설명:</td>
																<td><input type="text" name="content" id="content"></td>
															</tr>
															<tr>
																<td>가격:</td>
																<td><input type="text"
																	onKeyup="this.value=this.value.replace(/[^0-9]/g,'');"
																	name="pprice" id="pprice"></td>
															</tr>

															<!-- div -->
															<tr>
																<div class="inputArea">
																	<td><label for="pimg">이미지</label></td>
																	<td><input type="file" id="pimg" name="file" /></td>
															</tr>

															<script>
																$("#pimg")
																		.change(
																				function() {
																					if (this.files
																							&& this.files[0]) {
																						var reader = new FileReader;
																						reader.onload = function(
																								data) {
																							$(
																									".select_img img")
																									.attr(
																											"src",
																											data.target.result)
																									.width(
																											500);
																						}
																						reader
																								.readAsDataURL(this.files[0]);
																					}
																				});
															</script>
															</div>
															<!-- div -->

														</table>
														<div class="select_img">
															<img src="" />
														</div>
														<a style="color: #dfdfdf;"><%=request.getRealPath("/")%></a>
														<br> <input type="submit" id="submit" value="등록하기">
													</form>

												</div>
											</div>
										</div>
										<div class="card">
											<div class="card-header" id="headingOne">
												<h5 class="mb-0">
													<button class="btn btn-link collapsed"
														data-toggle="collapse" data-target="#collapseOne"
														aria-expanded="false" aria-controls="collapseOne">
														상품 정보</button>
												</h5>
											</div>
											<div id="collapseOne" class="collapse"
												aria-labelledby="headingOne" data-parent="#accordion"
												style="">
												<div class="card-body">
													<table class="type10">
														<tr>
															<th>상품 이름</th>
															<th>상품 코드</th>
															<th>브랜드명</th>
															<th>가격</th>
															<th>등록일</th>
															<th>상품 삭제</th>
														</tr>
														<c:forEach var="plist" items="${plist }">
															<tr>
																<td>${plist.pname}</td>
																<td>${plist.pcode}</td>
																<td>${plist.pbrand}</td>
																<td><fmt:formatNumber value="${plist.pprice}"
																		pattern="#,###.##" /></td>
																<td>${plist.pdate}</td>
																<td><a
																	onclick="location.href='poption/del/${plist.pcode }'"
																	style="cursor: pointer; color: #bfbcbc;">[삭제]</a></td>
															</tr>
														</c:forEach>
													</table>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- service_details_start  -->

	<!-- footer -->
	<div id="footer">
		<jsp:include page="../footer.jsp" />
	</div>
	<!-- /footer -->
</body>
</html>
