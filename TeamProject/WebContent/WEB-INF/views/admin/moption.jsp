<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>고객 관리</title>
<style>
table.type10 {
  border-spacing: 1px;
  text-align: center;
  line-height: 1.5;
  margin: 20px 10px;
}
table.type10 th {
  width: 155px;
  padding: 10px;
  font-weight: bold;
  vertical-align: center;
  color: #fff;
  background: #e0daf1 ;
}
table.type10 td {
  width: 155px;
  padding: 10px;
  font-size: 12px;
  vertical-align: center;
  border-bottom: 1px solid #ccc;
  background: #ffffff;
}
</style>
</head>
<body>
	<!-- header -->
	<div id="header">
		<jsp:include page="../header.jsp"/>
	</div>
	<!-- /header -->
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>고객 관리</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->

	<!-- service_details_start  -->
	<div class="service_details_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="service_details_left">
						<h3>관리자 모드</h3>
						<div class="nav nav-pills" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">
							<a class="" id="v-pills-profile-tab" href="/TeamProject/poption"
								role="tab" aria-controls="v-pills-profile" aria-selected="false">상품
								관리 </a> <a class="active" id="v-pills-home-tab"
								href="/TeamProject/moption" role="tab" data-toggle="pill"
								aria-controls="v-pills-homev-pills-profile" aria-selected="true">고객
								관리 </a>
								<a class=""
								id="v-pills-profile-tab" href="/TeamProject/ooption" role="tab"
								aria-controls="v-pills-profile" aria-selected="false">주문 관리
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home"
							role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="service_details_info">
								<h3>고객 정보</h3>
								<table class="type10">
									<tr>
										<th>아이디</th>
										<th>비밀번호</th>
										<th>이름</th>
										<th>생년월일</th>
										<th>휴대폰번호</th>
										<th>성별</th>
										<th>이메일</th>
										<th>주소</th>
										<th>삭제</th>
									</tr>
									<c:forEach var="mlist" items="${mlist }">
										<tr>
											<td>${mlist.id}</td>
											<td>${mlist.password}</td>
											<td>${mlist.name}</td>
											<td>${mlist.birthday}</td>
											<td>${mlist.phone}</td>
											<td>${mlist.gender}</td>
											<td>${mlist.email}</td>
											<td>${mlist.address}</td>
											<td><a onclick="location.href='moption/del/${mlist.id }'" style="cursor: pointer; color:#bfbcbc;">[삭제]</a></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- service_details_start  -->

	<!-- footer -->
	<div id="footer">
		<jsp:include page="../footer.jsp"/>
	</div>
	<!-- /footer -->
</body>
</html>