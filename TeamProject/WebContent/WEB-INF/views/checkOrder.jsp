<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>결제</title>
</head>
<body>
	<div id="header.jsp">
		<jsp:include page="header.jsp"></jsp:include>
	</div>
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>결제</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="sample-text-area">
		<div class="container box_1170">
			<div class="section-top-border">
				<div>
					<div class="table-head" style="background-color: #EFEFFB">
						<div class="serial">
							<p>
								<br> <i class="ti-angle-right"></i> <strong>상품 정보</strong><br>
							</p>
						</div>
						<div class="progress-table">
							<div class="table-head">
								<div class="serial"></div>
								<div class="visit">상품명</div>
								<div class="serial"></div>
								<div class="visit">수량</div>
								<div class="serial"></div>
								<div class="visit">가격</div>
							</div>
							<c:forEach var="cartList" items="${cartList}">
								<div class="table-row">
									<div class="serial"></div>
									<div class="visit">
										<img src="<spring:url value='../images/${productList.pimg}'/>">${cartList.pname}
									</div>
									<div class="serial"></div>
									<div class="country">
										<div class="cnt input">
											<input class="single-input" type="number" id="numBox[]"
												min="1" value="${cartList.cquantity}" readonly="readonly" />
										</div>
									</div>
									<div class="visit">
										<fmt:formatNumber pattern="###,###,###"
											value="${cartList.cprice}" />
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				<!-- 결제 금액  -->
				<form role="form" action="/TeamProject/pay" autocomplete="off">
					<div>
						<div class="table-head" style="background-color: #EFEFFB">
							<div class="serial">
								<p>
									<br> <i class="ti-angle-right"></i> <strong>결제 금액</strong><br>
								</p>
							</div>
							<div class="progress-table">
								<div class="table-row">
									<div class="serial"></div>
									<div class="visit">총 상품 금액</div>
									<div class="serial"></div>
									<div class="country"></div>
									<div class="visit">
										<p id="oprice">
											<fmt:formatNumber pattern="###,###,###" value="${cartSum}" />
										</p>
									</div>
								</div>
								<div class="table-row">
									<div class="serial"></div>
									<div class="visit">총 배송비</div>
									<div class="serial"></div>
									<div class="country"></div>
									<div class="visit">2,500</div>
								</div>
								<div class="table-row" style="color: #3C5A91; font-weight: 500;">
									<div class="serial"></div>
									<div class="visit">총 결제 금액</div>
									<div class="serial"></div>
									<div class="country"></div>
									<div class="visit">
										<fmt:formatNumber pattern="###,###,###"
											value="${cartSum+2500}" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 배송지 정보 -->
					<div>
						<div class="table-head" style="background-color: #EFEFFB">
							<div class="serial">
								<p>
									<br> <i class="ti-angle-right"></i> <strong>배송지
										정보</strong><br>
								</p>
							</div>
							<div class="progress-table">
								<div class="table-row" style="margin-left: 400px;">
									<div class="orderInfo">


										<input type="hidden" name="amount" value="0" />

										<div class="input-group-icon mt-12">
											<div class="icon">
												<i class="fa fa-thumb-tack" aria-hidden="true"></i>
											</div>
											<input id="orec" type="text" name="orec"
												placeholder="Recipient" onfocus="this.placeholder = ''"
												onblur="this.placeholder = 'Recipient'" required
												class="single-input" style="background-color: white;">
										</div>

										<div class="input-group-icon mt-12">
											<div class="icon">
												<i class="fa fa-thumb-tack" aria-hidden="true"></i>
											</div>
											<input id="ophone" type="number" name="ophone"
												placeholder="Phone number" onfocus="this.placeholder = ''"
												onblur="this.placeholder = 'Phone number'" required
												class="single-input" style="background-color: white;">
										</div>
										<div class="input-group-icon mt-12">
											<div class="icon">
												<i class="fa fa-globe" aria-hidden="true"></i>
											</div>
											<input id="oaddress" type="text" name="oaddress"
												placeholder="Address" onfocus="this.placeholder = ''"
												onblur="this.placeholder = 'Address'" required
												class="single-input" style="background-color: white;">
										</div>

										<div class="input-group-icon mt-12">
											<div class="icon">
												<i class="fa fa-plane" aria-hidden="true"></i>
											</div>
											<div class="form-select" id="default-select">
												<select>
													<option selected>배송메모를 선택해주세요.</option>
													<option value="1">배송 전에 미리 연락 바랍니다.</option>
													<option value="2">집 앞에 놔주세요.</option>
													<option value="3">경비실에 맡겨주세요.</option>
													<option value="4">택배함에 놔주세요.</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- 결제 방법 -->
					<div>
						<div class="table-head" style="background-color: #EFEFFB">
							<div class="serial">
								<p>
									<br> <i class="ti-angle-right"></i> <strong>결제 방식</strong><br>
								</p>
							</div>
							<div class="progress-table">
								<div class="table-row">

									<div class="table-head">결제 방식을 선택하세요.</div>
								</div>
							</div>
						</div>
					</div>
					<!-- 결제 버튼 -->
					<div class="button-group-area mt-40">
						<button type="submit" class="genric-btn info radius">주문하기</button>
					</div>

				</form>
			</div>
		</div>

		<div id="footer">
			<jsp:include page="footer.jsp"></jsp:include>
		</div>
	</section>
</body>
</html>