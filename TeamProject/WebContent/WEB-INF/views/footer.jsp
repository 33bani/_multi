<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>footer</title>
</head>
<body>
	<!-- contact_location  -->
	<div class="contact_location">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="location_left">
						<div class="logo">
							<a href="/TeamProject/main"> <img src="img/logo2.png" alt="">
							</a>
						</div>
						<ul>
							<li><a href="https://www.facebook.com/"> <i
									class="fa fa-facebook"></i>
							</a></li>
							<li><a href="https://www.google.com/"> <i
									class="fa fa-google-plus"></i>
							</a></li>
							<li><a href="https://twitter.com/?lang=ko"> <i
									class="fa fa-twitter"></i>
							</a></li>
							<li><a href="https://www.youtube.com/premium"> <i
									class="fa fa-youtube"></i>
							</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="img/icon/address.svg" alt=""> Location
						</h3>
						<p>서울특별시 강남구 선릉로 428 (대치동 889-41) 멀티캠퍼스 선릉</p>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="img/icon/support.svg" alt=""> 1588-1122
						</h3>
						<p>
							평일: 09:00 ~ 18:00 <br> 주말 · 공휴일: 휴무
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--/ contact_location  -->


	<!-- footer start -->
	<footer class="footer">
		<div class="copy-right_text">
			<div class="container">
				<div class="footer_border"></div>
				<div class="row">
					<div class="col-xl-12">
						<p class="copy_right text-center">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;
							<script>
								document.write(new Date().getFullYear());
							</script>
							| A time of happiness <i class="fa fa-heart-o" aria-hidden="true"></i>
							<a href="https://colorlib.com" target="_blank">일석이조</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/ footer end  -->

	<!-- Modal -->
	<div class="modal fade custom_search_pop" id="exampleModalCenter"
		tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="serch_form">
					<input type="text" placeholder="search">
					<button type="submit">search</button>
				</div>
			</div>
		</div>
	</div>

	<!-- JS here -->
	<script src="js/vendor/modernizr-3.5.0.min.js"></script>
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/isotope.pkgd.min.js"></script>
	<script src="js/ajax-form.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<script src="js/scrollIt.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/nice-select.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/plugins.js"></script>
	<!-- <script src="js/gijgo.min.js"></script> -->
	<script src="js/slick.min.js"></script>



	<!--contact js-->
	<script src="js/contact.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.form.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/mail-script.js"></script>


	<script src="js/main.js"></script>
</body>
</html>