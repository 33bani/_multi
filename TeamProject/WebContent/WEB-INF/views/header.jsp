<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>header</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/x-icon" href="/img/favicon2.ico">
<!-- CSS here -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/themify-icons.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet" href="css/flaticon.css">
<link rel="stylesheet" href="css/gijgo.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slicknav.css">
<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="css/style.css">
</head>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(
		function() {
			$("#submit1").on(
					"click",
					function() {
							location.href="/TeamProject/productListSearch/"+$("#search").val();
					});
		})
</script>
</head>
<body>
<!-- header-start -->
	<header>
		<div class="header-area ">
			<div class="header-top_area d-none d-lg-block">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-xl-4 col-lg-4">
							<div class="logo">
								<a href="/TeamProject/main"> <img src="img/logo2.png" alt="">
								</a>
							</div>
						</div>
						<div class="col-xl-8 col-md-8">
							<div class="header_right d-flex align-items-center">
								<div class="short_contact_list">
									<ul>
										<c:if test="${member == null}">
											<li><a href="/TeamProject/login">로그인</a></li>
											<li><a href="/TeamProject/signup">회원가입</a></li>
										</c:if>
										<c:if test="${member != null && member.id != 'admin'}">
											<ul>
												<li><a>${member.id}님 환영 합니다.</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
												<li><a href="/TeamProject/memberInfo">마이페이지</a></li>
												<li><a href="/TeamProject/cartList">장바구니</a></li>
											</ul>
										</c:if>
										<c:if test="${member.id == 'admin'}">
											<ul>
												<li><a>${member.id}관리자 님</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
											</ul>
										</c:if>
									</ul>
								</div>

								<div class="book_btn d-none d-lg-block">
									<a class="boxed-btn3-line" href="/TeamProject/review">상품후기</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sticky-header" class="main-header-area">
				<div class="container">
					<div class="header_bottom_border">
						<div class="row align-items-center">
							<div class="col-xl-9 col-lg-9">
								<div class="main-menu  d-none d-lg-block">
									<nav>
										<ul id="navigation">
											<!-- <li><a href="#">카테고리 <i class="ti-angle-down"></i></a>
												<ul class="submenu">
													<li><a href="#">사료/간식</a></li>
													<li><a href="#">위생/배변</a></li>
													<li><a href="#">미용/목욕</a></li>
													<li><a href="#">장난감</a></li>
												</ul></li> -->
											<li><a href="/TeamProject/main">HOME</a></li>
											<li><a href="/TeamProject/productList">ALL</a></li>
											<li><a href="/TeamProject/productList/dog">DOG</a></li>
											<li><a href="/TeamProject/productList/cat">CAT</a></li>
											<c:if test="${member.id == 'admin'}">
												<li><a href="#"> 관리자 페이지 <i class="ti-angle-down"></i></a>
													<ul class="submenu">
														<li><a href="/TeamProject/poption">상품 관리</a></li>
														<li><a href="/TeamProject/moption">고객 관리</a></li>
														<li><a href="/TeamProject/ooption">주문 관리</a></li>
													</ul>
												</li>
											</c:if>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 d-none d-lg-block">
								<div class="Appointment justify-content-end">
									<div class="search_btn">
										<a data-toggle="modal" data-target="#exampleModalCenter"
											href="#"> <i class="ti-search"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="mobile_menu d-block d-lg-none"></div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>
	<!-- header-end -->
	 <!-- Modal -->
  <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="serch_form">
            <input type="text" id="search" name="search" placeholder="search" >
            <button type="submit" id="submit1" >search</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>