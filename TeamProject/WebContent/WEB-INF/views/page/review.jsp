<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp"></jsp:include>
	</div>
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center" style="color: white">
						<h3>상품 후기</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container box_1170">
			<div class="section-top-border" style="text-align: center">
				등록된 후기가 없습니다. <br> <span style="cursor: pointer"
					onMouseover="this.style.color='darkblue';"
					onMouseout="this.style.color='black';"
					onclick="location.href='/TeamProject/main';">[홈페이지로 이동] </span>
				<div id="footer"></div>
			</div>
		</div>

		<jsp:include page="../footer.jsp"></jsp:include>
	</div>
</body>
</html>