<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>제품 상세</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/x-icon" href="../img/favicon2.ico">
<!-- CSS here -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/owl.carousel.min.css">
<link rel="stylesheet" href="../css/magnific-popup.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/themify-icons.css">
<link rel="stylesheet" href="../css/nice-select.css">
<link rel="stylesheet" href="../css/flaticon.css">
<link rel="stylesheet" href="../css/gijgo.css">
<link rel="stylesheet" href="../css/animate.css">
<link rel="stylesheet" href="../css/slick.css">
<link rel="stylesheet" href="../css/slicknav.css">
<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="../css/style.css">
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

		$(function() {
			$("#plus").click(function() {
				var num = $("#numBox").val();
				++num;
				$("#numBox").val(num);
				$("#totalPrice").val(num*${product.pprice});
			});
			
			$("#minus").click(function() {
				var num = $("#numBox").val();
				--num;
				if (num <= 0) {
					$("#numBox").val(1);
					$("#totalPrice").val(${product.pprice});
					alert("최소 1개부터 주문 가능합니다.");
				}
				else {
					$("#numBox").val(num);
					$("#totalPrice").val(num*${product.pprice});
				}
			});
			
			$("#addCart").click(function() {
				var id = "${member.id}";
				var pcode = ${product.pcode};
				var quantity = $("#numBox").val();
				var price = quantity * ${product.pprice};
				var data = {
					cid : id,
					cpcode : pcode,
					cquantity : quantity,
					cprice : price,
				};
				
				$.ajax({
					url : "/TeamProject/addCart",
					type : "post",
					data : data,
					success : function(result) {
						if (result == 1) {
							alert("장바구니에 담기 성공!");
						} else if (result == 2) {
							alert("장바구니에 담은 상품입니다.");
						} else {
							alert("로그인이 필요합니다.");
						}
						$("numBox").val(1);
					}
				});
			});
		});
	</script>
	<style>
table.type10 {
	border-spacing: 1px;
	text-align: center;
	line-height: 1.5;
	margin: 20px 10px;
}

table.type10 th {
	width: 155px;
	padding: 10px;
	font-weight: bold;
	vertical-align: center;
	color: #fff;
	background: #e0daf1;
}

table.type10 td {
	width: 155px;
	padding: 10px;
	font-size: 12px;
	vertical-align: center;
	border-bottom: 1px solid #ccc;
	background: #ffffff;
}
</style>
</head>
<body>

	<!-- header-start -->
	<header>
		<div class="header-area ">
			<div class="header-top_area d-none d-lg-block">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-xl-4 col-lg-4">
							<div class="logo">
								<a href="/TeamProject/main"> <img src="../img/logo2.png"
									alt="">
								</a>
							</div>
						</div>
						<div class="col-xl-8 col-md-8">
							<div class="header_right d-flex align-items-center">
								<div class="short_contact_list">
									<ul>
										<c:if test="${member == null}">
											<li><a href="/TeamProject/login">로그인</a></li>
											<li><a href="/TeamProject/signup">회원가입</a></li>
										</c:if>
										<c:if test="${member != null && member.id != 'admin'}">
											<ul>
												<li><a>${member.id}님 환영 합니다.</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
												<li><a href="/TeamProject/memberInfo">마이페이지</a></li>
												<li><a href="/TeamProject/cartList">장바구니</a></li>
											</ul>
										</c:if>
										<c:if test="${member.id == 'admin'}">
											<ul>
												<li><a>${member.id}관리자 님</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
											</ul>
										</c:if>
									</ul>
								</div>

								<div class="book_btn d-none d-lg-block">
									<a class="boxed-btn3-line" href="/TeamProject/review">상품후기</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sticky-header" class="main-header-area">
				<div class="container">
					<div class="header_bottom_border">
						<div class="row align-items-center">
							<div class="col-xl-9 col-lg-9">
								<div class="main-menu  d-none d-lg-block">
									<nav>
										<ul id="navigation">
											<!-- <li><a href="#">카테고리 <i class="ti-angle-down"></i></a>
												<ul class="submenu">
													<li><a href="#">사료/간식</a></li>
													<li><a href="#">위생/배변</a></li>
													<li><a href="#">미용/목욕</a></li>
													<li><a href="#">장난감</a></li>
												</ul></li> -->
											<li><a href="/TeamProject/main">HOME</a></li>
											<li><a href="/TeamProject/productList">ALL</a></li>
											<li><a href="/TeamProject/productList/dog">DOG</a></li>
											<li><a href="/TeamProject/productList/cat">CAT</a></li>
											<c:if test="${member.id == 'admin'}">
												<li><a href="#"> 관리자 페이지 <i class="ti-angle-down"></i></a>
													<ul class="submenu">
														<li><a href="/TeamProject/poption">상품 관리</a></li>
														<li><a href="/TeamProject/moption">고객 관리</a></li>
													</ul></li>
											</c:if>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 d-none d-lg-block">
								<div class="Appointment justify-content-end">
									<div class="search_btn">
										<a data-toggle="modal" data-target="#exampleModalCenter"
											href="#"> <i class="ti-search"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="mobile_menu d-block d-lg-none"></div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>
	<!-- header-end -->
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>제품 페이지</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->

	<!-- Start Align Area -->
	<div class="whole-wrap">
		<div class="container box_1170">
			<div class="section-top-border">
				<h3 class="mb-30">[${product.pbrand}] ${product.pname}</h3>
				<div class="row">
					<div class="col-md-3">
						<img width="250" src="<spring:url value='/images/${product.pimg}'/>">
					</div>
					<div class="col-md-9 mt-sm-20">
						<table class="type10">
							<tbody>
								<tr>
									<td>상품코드</td>
									<td>${product.pcode}</td>
								</tr>
								<tr>
									<td>브랜드</td>
									<td>${product.pbrand}</td>
								</tr>
								<tr>
									<td>판매가</td>
									<td><fmt:formatNumber value="${product.pprice}"
											pattern="#,###.##" /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<div class="mb-30">
				<hr />
				<p class="cartStock">
					<span>구입 수량</span> <input type="button" id="minus" value="-" /> <input
						type="number" id="numBox" min="1" value="1" readonly="readonly" />
					<input type="button" id="plus" value="+" />
				</p>
			</div>
			<div class="totalPrice">
				<hr />
				<span>총 합계금액<input type="text" class="no-border"
					id="totalPrice" value="${product.pprice}" dir="rtl"></span>

				<hr />
				<input type="button" id="addCart" class="genric-btn success large" value="장바구니 담기">
			</div>
			</div>
		</div>
	</div>
	<!-- contact_location  -->
	<div class="contact_location">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="location_left">
						<div class="logo">
							<a href="/TeamProject/main"> <img src="../img/logo2.png"
								alt="">
							</a>
						</div>
						<ul>
							<li><a href="https://www.facebook.com/"> <i
									class="fa fa-facebook"></i>
							</a></li>
							<li><a href="https://www.google.com/"> <i
									class="fa fa-google-plus"></i>
							</a></li>
							<li><a href="https://twitter.com/?lang=ko"> <i
									class="fa fa-twitter"></i>
							</a></li>
							<li><a href="https://www.youtube.com/premium"> <i
									class="fa fa-youtube"></i>
							</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="../img/icon/address.svg" alt=""> Location
						</h3>
						<p>서울특별시 강남구 선릉로 428 (대치동 889-41) 멀티캠퍼스 선릉</p>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="../img/icon/support.svg" alt=""> 1588-1122
						</h3>
						<p>
							평일: 09:00 ~ 18:00 <br> 주말 · 공휴일: 휴무
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--/ contact_location  -->


	<!-- footer start -->
	<footer class="footer">
		<div class="copy-right_text">
			<div class="container">
				<div class="footer_border"></div>
				<div class="row">
					<div class="col-xl-12">
						<p class="copy_right text-center">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;
							<script>
								document.write(new Date().getFullYear());
							</script>
							| A time of happiness <i class="fa fa-heart-o" aria-hidden="true"></i>
							<a href="https://colorlib.com" target="_blank">일석이조</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/ footer end  -->

	<!-- Modal -->
	<div class="modal fade custom_search_pop" id="exampleModalCenter"
		tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="serch_form">
					<input type="text" placeholder="search">
					<button type="submit">search</button>
				</div>
			</div>
		</div>
	</div>
	<!-- JS here -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
	<script src="../js/vendor/jquery-1.12.4.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.carousel.min.js"></script>
	<script src="../js/isotope.pkgd.min.js"></script>
	<script src="../js/ajax-form.js"></script>
	<script src="../js/waypoints.min.js"></script>
	<script src="../js/jquery.counterup.min.js"></script>
	<script src="../js/imagesloaded.pkgd.min.js"></script>
	<script src="../js/scrollIt.js"></script>
	<script src="../js/jquery.scrollUp.min.js"></script>
	<script src="../js/wow.min.js"></script>
	<script src="../js/nice-select.min.js"></script>
	<script src="../js/jquery.slicknav.min.js"></script>
	<script src="../js/jquery.magnific-popup.min.js"></script>
	<script src="../js/plugins.js"></script>
	<!-- <script src="js/gijgo.min.js"></script> -->
	<script src="../js/slick.min.js"></script>



	<!--contact js-->
	<script src="<c:url value="../js/contact.js"/>"></script>
	<script src="<c:url value="../js/jquery.ajaxchimp.min.js"/>"></script>
	<script src="<c:url value="../js/jquery.form.js"/>"></script>
	<script src="<c:url value="../js/jquery.validate.min.js"/>"></script>
	<script src="<c:url value="../js/mail-script.js"/>"></script>


	<script src="../js/main.js"></script>
</body>
</html>