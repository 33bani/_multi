<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>NEW</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="manifest" href="site.webmanifest"> -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
<!-- Place favicon.ico in the root directory -->

<!-- CSS here -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/themify-icons.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet" href="css/flaticon.css">
<link rel="stylesheet" href="css/gijgo.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slicknav.css">
<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="css/style.css">
<!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>
<body>
	<!-- header-start -->
	<header>
		<div class="header-area ">
			<div class="header-top_area d-none d-lg-block">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-xl-4 col-lg-4">
							<div class="logo">
								<a href="/TeamProject/main"> <img src="img/logo.png" alt="">
								</a>
							</div>
						</div>
						<div class="col-xl-8 col-md-8">
							<div class="header_right d-flex align-items-center">
								<div class="short_contact_list">
									<ul>
										<c:if test="${member == null}">
											<li><a href="/TeamProject/login">로그인</a></li>
											<li><a href="/TeamProject/signup">회원가입</a></li>
											<li><a href="/TeamProject/cartList">장바구니</a></li>
										</c:if>
										<c:if test="${member != null && member.id != 'admin'}">
											<ul>
												<li><a>${member.id}님 환영 합니다.</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
												<li><a href="/TeamProject/mypage">마이페이지</a></li>
												<li><a href="/TeamProject/cartList">장바구니</a></li>
											</ul>
										</c:if>
										<c:if test="${member.id == 'admin'}">
											<ul>
												<li><a>${member.id}관리자 님</a></li>
											</ul>
											<ul>
												<li><a href="/TeamProject/logout">로그아웃</a></li>
											</ul>
										</c:if>
									</ul>
								</div>

								<div class="book_btn d-none d-lg-block">
									<a class="boxed-btn3-line" href="#">상품후기</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="sticky-header" class="main-header-area">
				<div class="container">
					<div class="header_bottom_border">
						<div class="row align-items-center">
							<div class="col-xl-9 col-lg-9">
								<div class="main-menu  d-none d-lg-block">
									<nav>
										<ul id="navigation">
											<!-- <li><a href="#">카테고리 <i class="ti-angle-down"></i></a>
												<ul class="submenu">
													<li><a href="#">사료/간식</a></li>
													<li><a href="#">위생/배변</a></li>
													<li><a href="#">미용/목욕</a></li>
													<li><a href="#">장난감</a></li>
												</ul></li> -->
											<li><a href="/TeamProject/main">HOME</a></li>
											<li><a href="/TeamProject/new">NEW</a></li>
											<li><a href="/TeamProject/dog">DOG</a></li>
											<li><a href="/TeamProject/cat">CAT</a></li>
											<c:if test="${member.id == 'admin'}">
												<li><a href="#"> 관리자 페이지 <i class="ti-angle-down"></i></a>
													<ul class="submenu">
														<li><a href="/TeamProject/poption">상품 관리</a></li>
														<li><a href="/TeamProject/moption">고객 관리</a></li>
													</ul>
												</li>
											</c:if>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-xl-3 col-lg-3 d-none d-lg-block">
								<div class="Appointment justify-content-end">
									<div class="search_btn">
										<a data-toggle="modal" data-target="#exampleModalCenter"
											href="#"> <i class="ti-search"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="mobile_menu d-block d-lg-none"></div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>
	<!-- header-end -->

	<!-- contact_location  -->
	<div class="contact_location">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="location_left">
						<div class="logo">
							<a href="/TeamProject/main"> <img src="img/logo.png" alt="">
							</a>
						</div>
						<ul>
							<li><a href="https://www.facebook.com/"> <i
									class="fa fa-facebook"></i>
							</a></li>
							<li><a href="https://www.google.com/"> <i
									class="fa fa-google-plus"></i>
							</a></li>
							<li><a href="https://twitter.com/?lang=ko"> <i
									class="fa fa-twitter"></i>
							</a></li>
							<li><a href="https://www.youtube.com/premium"> <i
									class="fa fa-youtube"></i>
							</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="img/icon/address.svg" alt=""> Location
						</h3>
						<p>서울특별시 강남구 선릉로 428 (대치동 889-41) 멀티캠퍼스 선릉</p>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="img/icon/support.svg" alt=""> 1588-1122
						</h3>
						<p>
							평일: 09:00 ~ 18:00 <br> 주말 · 공휴일: 휴무
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--/ contact_location  -->
	<!-- footer start -->
	<footer class="footer">
		<div class="copy-right_text">
			<div class="container">
				<div class="footer_border"></div>
				<div class="row">
					<div class="col-xl-12">
						<p class="copy_right text-center">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;
							<script>
								document.write(new Date().getFullYear());
							</script>
							| A time of happiness <i class="fa fa-heart-o" aria-hidden="true"></i>
							<a href="https://colorlib.com" target="_blank">일석이조</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/ footer end  -->

	<!-- Modal -->
	<div class="modal fade custom_search_pop" id="exampleModalCenter"
		tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="serch_form">
					<input type="text" placeholder="search">
					<button type="submit">search</button>
				</div>
			</div>
		</div>
	</div>

	<!-- JS here -->
	<script src="js/vendor/modernizr-3.5.0.min.js"></script>
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/isotope.pkgd.min.js"></script>
	<script src="js/ajax-form.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/jquery.counterup.min.js"></script>
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<script src="js/scrollIt.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/nice-select.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/plugins.js"></script>
	<!-- <script src="js/gijgo.min.js"></script> -->
	<script src="js/slick.min.js"></script>



	<!--contact js-->
	<script src="js/contact.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.form.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/mail-script.js"></script>


	<script src="js/main.js"></script>
</body>
</html>