<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>목록</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/x-icon" href="../img/favicon2.ico">
<!-- CSS here -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/owl.carousel.min.css">
<link rel="stylesheet" href="../css/magnific-popup.css">
<link rel="stylesheet" href="../css/font-awesome.min.css">
<link rel="stylesheet" href="../css/themify-icons.css">
<link rel="stylesheet" href="../css/nice-select.css">
<link rel="stylesheet" href="../css/flaticon.css">
<link rel="stylesheet" href="../css/gijgo.css">
<link rel="stylesheet" href="../css/animate.css">
<link rel="stylesheet" href="../css/slick.css">
<link rel="stylesheet" href="../css/slicknav.css">
<link rel="stylesheet"
	href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="../css/style.css">
</head>
<body>
<div id="header">
<jsp:include page="../header.jsp"></jsp:include>
</div>
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>
							<c:choose>
								<c:when test="${productList[0].pcode >= 20000}">
									고양이상품 </c:when>
								<c:otherwise>강아지상품 </c:otherwise>
							</c:choose>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ bradcam_area  -->

	<!-- service_area  -->
	<div class="service_area" style="background-color: white;">
		<div class="container">
			<div class="row">
				<c:forEach var="productList" items="${productList}">
					<div class="col-md-6 col-lg-4">
						<div class="single_service">
							<div class="thumb"
								onclick="location.href='/TeamProject/productDetail/${productList.pcode}'">
								<img src="<spring:url value='/images/${productList.pimg}'/>">
							</div>
							<div class="service_info"
								onclick="location.href='/TeamProject/productDetail/${productList.pcode}'">
								<h3>
									<a href="/TeamProject/productDetail/${productList.pcode}">[${productList.pbrand}]
										${productList.pname}</a>
								</h3>
								<p>
									<fmt:formatNumber value="${productList.pprice}"
										pattern="###,###" />
									원
								</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<!-- contact_location  -->
	<div class="contact_location">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="location_left">
						<div class="logo">
							<a href="/TeamProject/main"> <img src="../img/logo2.png"
								alt="">
							</a>
						</div>
						<ul>
							<li><a href="https://www.facebook.com/"> <i
									class="fa fa-facebook"></i>
							</a></li>
							<li><a href="https://www.google.com/"> <i
									class="fa fa-google-plus"></i>
							</a></li>
							<li><a href="https://twitter.com/?lang=ko"> <i
									class="fa fa-twitter"></i>
							</a></li>
							<li><a href="https://www.youtube.com/premium"> <i
									class="fa fa-youtube"></i>
							</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="../img/icon/address.svg" alt=""> Location
						</h3>
						<p>서울특별시 강남구 선릉로 428 (대치동 889-41) 멀티캠퍼스 선릉</p>
					</div>
				</div>
				<div class="col-xl-3 col-md-3">
					<div class="single_location">
						<h3>
							<img src="../img/icon/support.svg" alt=""> 1588-1122
						</h3>
						<p>
							평일: 09:00 ~ 18:00 <br> 주말 · 공휴일: 휴무
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--/ contact_location  -->


	<!-- footer start -->
	<footer class="footer">
		<div class="copy-right_text">
			<div class="container">
				<div class="footer_border"></div>
				<div class="row">
					<div class="col-xl-12">
						<p class="copy_right text-center">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;
							<script>
								document.write(new Date().getFullYear());
							</script>
							| A time of happiness <i class="fa fa-heart-o" aria-hidden="true"></i>
							<a href="https://colorlib.com" target="_blank">일석이조</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--/ footer end  -->

	<!-- Modal -->
	<div class="modal fade custom_search_pop" id="exampleModalCenter"
		tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="serch_form">
					<input type="text" placeholder="search">
					<button type="submit">search</button>
				</div>
			</div>
		</div>
	</div>
	<!-- JS here -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
	<script src="../js/vendor/jquery-1.12.4.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/owl.carousel.min.js"></script>
	<script src="../js/isotope.pkgd.min.js"></script>
	<script src="../js/ajax-form.js"></script>
	<script src="../js/waypoints.min.js"></script>
	<script src="../js/jquery.counterup.min.js"></script>
	<script src="../js/imagesloaded.pkgd.min.js"></script>
	<script src="../js/scrollIt.js"></script>
	<script src="../js/jquery.scrollUp.min.js"></script>
	<script src="../js/wow.min.js"></script>
	<script src="../js/nice-select.min.js"></script>
	<script src="../js/jquery.slicknav.min.js"></script>
	<script src="../js/jquery.magnific-popup.min.js"></script>
	<script src="../js/plugins.js"></script>
	<!-- <script src="js/gijgo.min.js"></script> -->
	<script src="../js/slick.min.js"></script>



	<!--contact js-->
	<script src="<c:url value="../js/contact.js"/>"></script>
	<script src="<c:url value="../js/jquery.ajaxchimp.min.js"/>"></script>
	<script src="<c:url value="../js/jquery.form.js"/>"></script>
	<script src="<c:url value="../js/jquery.validate.min.js"/>"></script>
	<script src="<c:url value="../js/mail-script.js"/>"></script>


	<script src="../js/main.js"></script>
</body>
</html>