<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>상품 목록</title>

</head>
<body>
	<div id="header">
		<jsp:include page="../header.jsp"></jsp:include>
	</div>
	<!-- bradcam_area  -->
	<div class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text text-center">
						<h3>상품 목록</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- service_area  -->
	<div class="service_area" style="background-color: white;">
		<div class="container">
			<div class="row">
				<c:forEach var="productList" items="${productList}">
					<div class="col-md-6 col-lg-4">
						<div class="single_service">
							<div class="thumb"
								onclick="location.href='/TeamProject/productDetail/${productList.pcode}'">
								<img src="<spring:url value='/images/${productList.pimg}'/>">
							</div>
							<div class="service_info"
								onclick="location.href='/TeamProject/productDetail/${productList.pcode}'">
								<h3>
									<a href="/TeamProject/productDetail/${productList.pcode}">[${productList.pbrand}]
										${productList.pname}</a>
								</h3>
								<p><fmt:formatNumber value="${productList.pprice}" pattern="###,###"/>원</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<div id="footer">
		<jsp:include page="../footer.jsp"></jsp:include>
	</div>
</body>
</html>