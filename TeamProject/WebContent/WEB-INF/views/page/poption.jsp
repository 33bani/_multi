<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>상품 등록 및 삭제</title>
</head>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#submit").on("click", function(){
		if($("#pname").val()==""){
			alert("상품이름을 입력해주세요.");
			$("#pname").focus();
			return false;
		}
		if($("#pbrand").val()==""){
			alert("브랜드명을 입력해주세요.");
			$("#pbrand").focus();
			return false;
		}
		if($("#pprice").val()==""){
			alert("가격을 입력해주세요.");
			$("#pprice").focus();
			return false;
		}
		alert("상품등록에 성공하였습니다.");
	});
})
</script>
<body>
<h1>상품 등록</h1>
<form action="addp" method="post" enctype="multipart/form-data">
<table>
	<tr>
		<td>상품 종류 :</td>
		<td>
		<input type="radio" name="code" value="10000" checked>강아지
		<input type="radio" name="code" value="20000">고양이
		</td>
	</tr>
	<tr>
		<td>상품 이름:</td>
		<td><input type="text" name="pname" id="pname"></td>
	</tr>
	<tr>
		<td>브랜드명:</td>
		<td><input type="text" name="pbrand" id="pbrand"></td>
	</tr>
	<tr>
		<td>상세 설명:</td>
		<td><input type="text" name="content" id="content"></td>
	</tr>
	<tr>
		<td>가격:</td>
		<td><input type="text" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" name="pprice" id="pprice"></td>
	</tr>
</table>
<div class="inputArea">
 <label for="pimg">이미지</label>
 <input type="file" id="pimg" name="file" />
 <div class="select_img"><img src="" /></div>
 
 <script>
  $("#pimg").change(function(){
   if(this.files && this.files[0]) {
    var reader = new FileReader;
    reader.onload = function(data) {
     $(".select_img img").attr("src", data.target.result).width(500);        
    }
    reader.readAsDataURL(this.files[0]);
   }
  });
 </script>
 <%=request.getRealPath("/") %>
</div>
<input type="submit" id="submit" value="등록하기">
</form>
<h1>상품 삭제</h1>
<table border="1">
	<tr>
		<th>상품 이름</th>
		<th>상품 코드</th>
		<th>브랜드명</th>
		<th>가격</th>
		<th>등록일</th>
	</tr>
	<c:forEach var="plist" items="${plist }">
	<tr>
		<td>${plist.pname}</td>
		<td>${plist.pcode}</td>
		<td>${plist.pbrand}</td>
		<td>${plist.pprice}</td>
		<td>${plist.pdate}</td>
		<td><button onclick="location.href='poption/del/${plist.pcode }'">삭제</button></td>
	</tr>
	</c:forEach>
</table>
<br>
<input type="button" onclick="location.href='/TeamProject/main'" value="메인으로 돌아가기">
</body>
</html>