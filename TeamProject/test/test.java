import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import teamproject.config.ApplicationConfig;
import teamproject.dto.Member;
import teamproject.dto.Product;
import teamproject.service.TeamProjectService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class})
public class test {
	@Autowired
	TeamProjectService teamProjectService;
	
	@Test
	public void memberadd() throws Exception {
		Member testmember = new Member();
		
		testmember.setId("민경준");
		testmember.setPassword("12345");
		
		int result = teamProjectService.pwCheck(testmember);
		System.out.println(result);
	}
}
