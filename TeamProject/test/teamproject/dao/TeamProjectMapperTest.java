package teamproject.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import teamproject.config.ApplicationConfig;
import teamproject.dto.Cart;
import teamproject.dto.Member;
import teamproject.dto.Porder;
import teamproject.dto.PorderDetail;
import teamproject.dto.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class})
public class TeamProjectMapperTest {
	@Autowired
	TeamProjectMapper teamprojectmapper;
	
	@Test
	public void login() throws Exception {
		Member testMember = new Member();
		testMember.setId("test2");
		testMember.setPassword("test2");
		Member member = teamprojectmapper.login(testMember);
		System.out.println(member);
	}
	
	@Test
	public void best() throws Exception {
		List<Product> test = teamprojectmapper.best();
		System.out.println(test);
	}
	
	@Test
	public void getProduct() throws Exception {
		System.out.println(teamprojectmapper.getProduct(10001));
	}
	
	@Test
	public void dogProduct() throws Exception {
		List<Product> dog = teamprojectmapper.dogProduct();
		System.out.println(dog);
	}
	@Test
	public void catProduct() throws Exception {
		List<Product> cat = teamprojectmapper.catProduct();
		System.out.println(cat);
	}
	@Test
	public void addCart() throws Exception {
		Cart testcart = new Cart();
		testcart.setCid("test");
		testcart.setCpcode(10003);
		testcart.setCquantity(10);
		teamprojectmapper.addCart(testcart);
	}
	@Test
	public void cartCheck() throws Exception {
		Cart testcart = new Cart();
		testcart.setCid("test");
		testcart.setCpcode(10003);
		teamprojectmapper.cartCheck(testcart);
	}
	@Test
	public void showCart() throws Exception {
		List<Cart> cartList = teamprojectmapper.showCart("test2");
		System.out.println(cartList);
	}
	
	@Test
	public void orderInfo() throws Exception {
		Porder testorder = new Porder();
		
		String subNum = "";
		for (int i = 1; i < 6; i++) {
			subNum += (int)(Math.random() * 10);
		}
		
		int ocode = Integer.parseInt(subNum);
		testorder.setOcode(ocode);
		testorder.setOcid("test2");
		teamprojectmapper.orderInfo(testorder);
	}
	
	@Test
	public void detailInfo() throws Exception {
		PorderDetail testdetail = new PorderDetail();
		testdetail.setOid(16134);
		teamprojectmapper.detailInfo(testdetail);
	}
	
	@Test
	public void orderList() throws Exception {
		List<PorderDetail> testorder = teamprojectmapper.orderList();
		System.out.println(testorder);
	}
	
	@Test
	public void sum() throws Exception {
		System.out.println(teamprojectmapper.sum("test2"));
	}
	@Test
	public void searchList() throws Exception {
		String pname = "test";
		List<Product> test = teamprojectmapper.searchProduct(pname);
		System.out.println(test);
	}
}
