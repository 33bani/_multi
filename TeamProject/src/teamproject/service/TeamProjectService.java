package teamproject.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import teamproject.dao.TeamProjectMapper;
import teamproject.dto.Cart;
import teamproject.dto.Member;
import teamproject.dto.Porder;
import teamproject.dto.PorderDetail;
import teamproject.dto.Product;

@Service
public class TeamProjectService {
	@Autowired
	private TeamProjectMapper teamprojectMapper;
	
	public void addMember(Member member) {
		teamprojectMapper.addMember(member);
	}
	public int idCheck(Member member) {
		int result = teamprojectMapper.idCheck(member);
		return result;
	}
	public int pwCheck(Member member) {
		int result = teamprojectMapper.pwCheck(member);
		return result;
	}
	public Member login(Member member) {
		return teamprojectMapper.login(member);
	}
	public List<Product> dogProduct() {
		return teamprojectMapper.dogProduct();
	}
	public List<Product> catProduct() {
		return teamprojectMapper.catProduct();
	}
	public List<Product> allProduct() {
		return teamprojectMapper.getProductList();
	}
	public List<Product> best() {
		return teamprojectMapper.best();
	}
	public Product getProduct(int pcode) {
		return teamprojectMapper.getProduct(pcode);
	}
	public int addCart(Cart cart) {
		return teamprojectMapper.addCart(cart);
	}
	public Cart cartCheck(Cart cart) {
		return teamprojectMapper.cartCheck(cart);
	}
	public List<Cart> showCart(String cid) {
		return teamprojectMapper.showCart(cid);
	}
	public int sum(String cid) {
		return teamprojectMapper.sum(cid);
	}
	public void addProduct(Product product) {
		teamprojectMapper.addProduct(product);
	}
	public List<Product> getProductList() {
		return teamprojectMapper.getProductList();
	}
	public void delProduct(int pcode) {
		teamprojectMapper.delProduct(pcode);
	}
	public List<Member> getMemberList() {
		return teamprojectMapper.getMemberList();
	}
	public void delMember(String id) {
		teamprojectMapper.delMember(id);
	}
	public void delCart(int cpcode) {
		teamprojectMapper.delCart(cpcode);
	}
	public int orderInfo(Porder porder) {
		return teamprojectMapper.orderInfo(porder);
	}
	public int detailInfo(PorderDetail porderDetail) {
		return teamprojectMapper.detailInfo(porderDetail);
	}
	public int deleteCartAll(String id) {
		return teamprojectMapper.deleteCartAll(id);
	}
	public List<PorderDetail> orderList() {
		return teamprojectMapper.orderList();
	}
	public void memberUpdate(Member member) {
		teamprojectMapper.memberUpdate(member);
	}
	public Member memberInfo(String id) {
		return teamprojectMapper.memberInfo(id);
	}
	public List<Product> searchProduct(String pname) {
		return teamprojectMapper.searchProduct(pname);
	}
}
