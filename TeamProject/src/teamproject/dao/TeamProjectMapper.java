package teamproject.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import teamproject.dto.Cart;
import teamproject.dto.Member;
import teamproject.dto.Porder;
import teamproject.dto.PorderDetail;
import teamproject.dto.Product;

@Mapper
public interface TeamProjectMapper {
	//회원가입
	public void addMember(Member member);
	//id확인
	public int idCheck(Member member);
	//pw확인
	public int pwCheck(Member member);
	//로그인
	public Member login(Member member);
	
	//강아지 버튼 클릭
	public List<Product> dogProduct();
	//고양이 버튼 클릭
	public List<Product> catProduct();
	//Best 상품
	public List<Product> best();
	//상품 클릭(상세보기)
	public Product getProduct(int pcode);
	//장바구니 담기
	public int addCart(Cart cart);
	//장바구니 중복검사
	public Cart cartCheck(Cart cart);
	//장바구니 리스트
	public List<Cart> showCart(String cid);
	public int sum(String id);
	public void addProduct(Product product);
	//상품 정보(관리자)
	public List<Product> getProductList();
	//상품 삭제(관리자)
	public void delProduct(int pcode);
	//고객 정보(관리자)
	public List<Member> getMemberList();
	//고객 삭제(관리자)
	public void delMember(String id);
	
	public void delCart(int cpcode);
	
	public int orderInfo(Porder porder);
	public int detailInfo(PorderDetail porderDetail);
	public int deleteCartAll(String id);
	public List<PorderDetail> orderList();
	
	//회원정보 수정
	public void memberUpdate(Member member);
	//회원 정보
	public Member memberInfo(String id);
	//회원 삭제
	public void memberDel(String id);
	
	public List<Product> searchProduct(String pname);
}
