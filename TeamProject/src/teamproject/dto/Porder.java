package teamproject.dto;

import java.util.Date;

public class Porder {
	private int ocode;
	private String ocid;
	private String orec;
	private String oaddress;
	private String ophone;
	private int oprice;
	private Date odate;
	private PorderDetail porderdetail;
	
	public int getOcode() {
		return ocode;
	}
	public void setOcode(int ocode) {
		this.ocode = ocode;
	}
	public String getOcid() {
		return ocid;
	}
	public void setOcid(String ocid) {
		this.ocid = ocid;
	}
	public String getOrec() {
		return orec;
	}
	public void setOrec(String orec) {
		this.orec = orec;
	}
	public String getOaddress() {
		return oaddress;
	}
	public void setOaddress(String oaddress) {
		this.oaddress = oaddress;
	}
	public String getOphone() {
		return ophone;
	}
	public void setOphone(String ophone) {
		this.ophone = ophone;
	}
	public int getOprice() {
		return oprice;
	}
	public void setOprice(int oprice) {
		this.oprice = oprice;
	}
	public Date getOdate() {
		return odate;
	}
	public void setOdate(Date odate) {
		this.odate = odate;
	}
	@Override
	public String toString() {
		return "Porder [ocode=" + ocode + ", ocid=" + ocid + ", orec=" + orec + ", oaddress=" + oaddress + ", ophone="
				+ ophone + ", oprice=" + oprice + ", odate=" + odate + ", porderdetail=" + porderdetail + "]";
	}
	public PorderDetail getPorderdetail() {
		return porderdetail;
	}
	public void setPorderdetail(PorderDetail porderdetail) {
		this.porderdetail = porderdetail;
	}
}