package teamproject.dto;

public class PorderDetail {
	private int odcode;
	private int oid;
	private int opcode;
	private int oquantity;
	private int qsum;
	private Porder porder;
	private Product product;
	
	public int getOdcode() {
		return odcode;
	}
	public void setOdcode(int odcode) {
		this.odcode = odcode;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getOpcode() {
		return opcode;
	}
	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}
	public int getOquantity() {
		return oquantity;
	}
	public void setOquantity(int oquantity) {
		this.oquantity = oquantity;
	}
	public int getQsum() {
		return qsum;
	}
	public void setQsum(int qsum) {
		this.qsum = qsum;
	}
	public Porder getPorder() {
		return porder;
	}
	public void setPorder(Porder porder) {
		this.porder = porder;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	@Override
	public String toString() {
		return "PorderDetail [odcode=" + odcode + ", oid=" + oid + ", opcode=" + opcode + ", oquantity=" + oquantity
				+ ", qsum=" + qsum + ", porder=" + porder + ", product=" + product + "]";
	}
}