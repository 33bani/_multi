package teamproject.dto;

import java.sql.Date;

public class Cart {
	private int ccode;
	private String cid;
	private int cpcode;
	private int cquantity;
	private int cprice;
	private Date cdate;
	private int cpsum;
	
	private String pname;
	private int pcode;
	private String pbrand;
	private int pprice;
	private Date pdate;
	private String pimg;

	public int getCcode() {
		return ccode;
	}
	public void setCcode(int ccode) {
		this.ccode = ccode;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public int getCpcode() {
		return cpcode;
	}
	public void setCpcode(int cpcode) {
		this.cpcode = cpcode;
	}
	public int getCquantity() {
		return cquantity;
	}
	public void setCquantity(int cquantity) {
		this.cquantity = cquantity;
	}
	public int getCprice() {
		return cprice;
	}
	public void setCprice(int cprice) {
		this.cprice = cprice;
	}
	public Date getCdate() {
		return cdate;
	}
	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	public int getCqsum() {
		return cpsum;
	}
	public void setCqsum(int cqsum) {
		this.cpsum = cqsum;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getPcode() {
		return pcode;
	}
	public void setPcode(int pcode) {
		this.pcode = pcode;
	}
	public String getPbrand() {
		return pbrand;
	}
	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}
	public int getPprice() {
		return pprice;
	}
	public void setPprice(int pprice) {
		this.pprice = pprice;
	}
	public Date getPdate() {
		return pdate;
	}
	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}
	public String getPimg() {
		return pimg;
	}
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}
	@Override
	public String toString() {
		return "Cart [ccode=" + ccode + ", cid=" + cid + ", cpcode=" + cpcode + ", cquantity=" + cquantity + ", cprice="
				+ cprice + ", cdate=" + cdate + ", cpsum=" + cpsum + ", pname=" + pname + ", pcode=" + pcode
				+ ", pbrand=" + pbrand + ", pprice=" + pprice + ", pdate=" + pdate + ", pimg=" + pimg + "]";
	}
	
	
}