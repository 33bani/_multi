package teamproject.dto;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Product {
	private String pname;
	private int pcode;
	private String pbrand;
	private int pprice;
	private Date pdate;
	private String content;
	private String pimg;
	private PorderDetail porderdetail;
	
	private int opcode;
	private int oquantity;
	private int qsum;
	
	
	public PorderDetail getPorderdetail() {
		return porderdetail;
	}
	public void setPorderdetail(PorderDetail porderdetail) {
		this.porderdetail = porderdetail;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getPcode() {
		return pcode;
	}
	public void setPcode(int pcode) {
		this.pcode = pcode;
	}
	public String getPbrand() {
		return pbrand;
	}
	public void setPbrand(String pbrand) {
		this.pbrand = pbrand;
	}
	public int getPprice() {
		return pprice;
	}
	public void setPprice(int pprice) {
		this.pprice = pprice;
	}
	public String getPdate() {
		SimpleDateFormat sf = new SimpleDateFormat("yy-MM-dd hh:mm");
		return sf.format(pdate);
	}
	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPimg() {
		return pimg;
	}
	public void setPimg(String pimg) {
		this.pimg = pimg;
	}
	public int getOpcode() {
		return opcode;
	}
	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}
	public int getOquantity() {
		return oquantity;
	}
	public void setOquantity(int oquantity) {
		this.oquantity = oquantity;
	}
	public int getQsum() {
		return qsum;
	}
	public void setQsum(int qsum) {
		this.qsum = qsum;
	}
	@Override
	public String toString() {
		return "Product [pname=" + pname + ", pcode=" + pcode + ", pbrand=" + pbrand + ", pprice=" + pprice + ", pdate="
				+ getPdate() + ", content=" + content + ", pimg=" + pimg + ", porderdetail=" + porderdetail + ", opcode="
				+ opcode + ", oquantity=" + oquantity + ", qsum=" + qsum + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + opcode;
		result = prime * result + oquantity;
		result = prime * result + ((pbrand == null) ? 0 : pbrand.hashCode());
		result = prime * result + pcode;
		result = prime * result + ((pdate == null) ? 0 : pdate.hashCode());
		result = prime * result + ((pimg == null) ? 0 : pimg.hashCode());
		result = prime * result + ((pname == null) ? 0 : pname.hashCode());
		result = prime * result + ((porderdetail == null) ? 0 : porderdetail.hashCode());
		result = prime * result + pprice;
		result = prime * result + qsum;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (opcode != other.opcode)
			return false;
		if (oquantity != other.oquantity)
			return false;
		if (pbrand == null) {
			if (other.pbrand != null)
				return false;
		} else if (!pbrand.equals(other.pbrand))
			return false;
		if (pcode != other.pcode)
			return false;
		if (pdate == null) {
			if (other.pdate != null)
				return false;
		} else if (!pdate.equals(other.pdate))
			return false;
		if (pimg == null) {
			if (other.pimg != null)
				return false;
		} else if (!pimg.equals(other.pimg))
			return false;
		if (pname == null) {
			if (other.pname != null)
				return false;
		} else if (!pname.equals(other.pname))
			return false;
		if (porderdetail == null) {
			if (other.porderdetail != null)
				return false;
		} else if (!porderdetail.equals(other.porderdetail))
			return false;
		if (pprice != other.pprice)
			return false;
		if (qsum != other.qsum)
			return false;
		return true;
	}

	
	
}