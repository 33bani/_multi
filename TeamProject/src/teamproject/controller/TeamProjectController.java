package teamproject.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import teamproject.dto.Cart;
import teamproject.dto.Member;
import teamproject.dto.Porder;
import teamproject.dto.PorderDetail;
import teamproject.dto.Product;
import teamproject.service.TeamProjectService;
import teamproject.utils.UploadFileUtils;

@Controller
@RequestMapping
public class TeamProjectController {
	@Autowired
	private TeamProjectService teamProjectService;

	// 회원가입
	@PostMapping("/add")
	public String addMember(@ModelAttribute Member member, HttpServletRequest request) {
		// 생년월일 받아오기
		String year = request.getParameter("birthyear");
		String month = request.getParameter("birthmonth");
		String day = request.getParameter("birthdate");
		String birth = year + "-" + month + "-" + day;
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date to = null;
		try {
			to = sf.parse(birth);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		member.setBirthday(to);

		// 전화번호 받아오기
		String phone1 = request.getParameter("phone1");
		String phone2 = request.getParameter("phone2");
		String phone3 = request.getParameter("phone3");
		String phone = phone1 + phone2 + phone3;
		member.setPhone(phone);

		teamProjectService.addMember(member);
		return "redirect:/main";
	}

	@GetMapping("/main")
	public String main(ModelMap model) {
		List<Product> best = teamProjectService.best();
//		List<Product> best5 = new ArrayList<Product>();
//		for (int i=0; i < 5; i++) {
//			best5.add(best.get(i));
//		}
		model.addAttribute("best", best);
		return "main";
	}

	@GetMapping("/review")
	public String view() {
		return "page/review";
	}

	@GetMapping("/signup")
	public String signup() {
		return "signup";
	}

	// ID 확인
	@ResponseBody
	@RequestMapping(value = "/idCheck", method = RequestMethod.POST)
	public int idCheck(Member member) {
		int result = teamProjectService.idCheck(member);
		return result;
	}

	// PW 확인
	@ResponseBody
	@RequestMapping(value = "/pwCheck", method = RequestMethod.POST)
	public int pwCheck(Member member) {
		int result = teamProjectService.pwCheck(member);
		return result;
	}

	// 로그인
	@GetMapping("/login")
	public String login() {
		return "login";
	}

	// 로그인 성공
	@RequestMapping(value = "/loginOK", method = { RequestMethod.GET, RequestMethod.POST })
	public String loginOK(Member member, HttpServletRequest req, RedirectAttributes rttr) {

		HttpSession session = req.getSession();
		Member login = teamProjectService.login(member);

		if (login == null) {
			session.setAttribute("member", null);
			rttr.addFlashAttribute("msg", false);
		} else {
			session.setAttribute("member", login);
		}
		return "redirect:/login";
	}

	// 로그아웃
	@RequestMapping(value = "/logout", method = { RequestMethod.GET, RequestMethod.POST })
	public String logOut(HttpSession session) {
		session.invalidate();
		return "redirect:/main";
	}

///////////
	// 전체 상품 조회
	@GetMapping("/productList")
	public String productAllView(ModelMap model) {
		model.addAttribute("productList", teamProjectService.getProductList());
		return "page/productAll";
	}

////////////
	// 상품 상세페이지
	@GetMapping("/productList/{type}")
	public String productView(@PathVariable("type") String type, ModelMap model, HttpServletRequest request) {
		if (type.equals("dog"))
			model.addAttribute("productList", teamProjectService.dogProduct());
		else 
			model.addAttribute("productList", teamProjectService.catProduct());
		return "page/productList";
	}

	// 상품 검색페이지
	@GetMapping("/productListSearch/{type}")
	public String productView1(@PathVariable("type") String type, ModelMap model) {
		model.addAttribute("search", type);
		model.addAttribute("productList", teamProjectService.searchProduct(type));
		return "page/productListSearch";
	}

	@GetMapping("/productDetail/{pcode}")
	public String getProduct(@PathVariable("pcode") int pcode, ModelMap model) {
		Product product = teamProjectService.getProduct(pcode);
		model.addAttribute("product", product);
		return "page/productDetail";
	}

	@ResponseBody
	@RequestMapping(value = "/addCart", method = RequestMethod.POST)
	public int addCart(Cart cart, HttpSession session) {
		// 로그인
		Member member = (Member) session.getAttribute("member");
		// 중복확인
		Cart cartCh = teamProjectService.cartCheck(cart);
		int result = 0;

		if (member != null) {
			if (cartCh == null) {
				cart.setCid(member.getId());
				teamProjectService.addCart(cart);
				result = 1;
			} else {
				result = 2;
			}
		}
		System.out.println(result);
		return result;
	}

	@GetMapping("/cartList")
	public String showCart(HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String cid = member.getId();
		model.addAttribute("cartList", teamProjectService.showCart(cid));
		return "member/cart";
	}

	@GetMapping("/checkOrder")
	public String checkOrder(HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String cid = member.getId();
		model.addAttribute("cartList", teamProjectService.showCart(cid));
		model.addAttribute("cartSum", teamProjectService.sum(cid));
		return "checkOrder";
	}

	@GetMapping("/pay")
	public String pay(HttpSession session, Porder porder, PorderDetail porderDetail) {
		Member member = (Member) session.getAttribute("member");
		String cid = member.getId();
		int oprice = teamProjectService.sum(cid);

		String subNum = "";
		for (int i = 1; i < 6; i++) {
			subNum += (int) (Math.random() * 10);
		}

		int ocode = Integer.parseInt(subNum);
		porder.setOcode(ocode);
		porder.setOcid(cid);
		porder.setOprice(oprice);
		teamProjectService.orderInfo(porder);
		porderDetail.setOid(ocode);
		teamProjectService.detailInfo(porderDetail);
		teamProjectService.deleteCartAll(cid);
		return "pay";
	}

	@GetMapping("/ooption")
	public String oOption(ModelMap model) {
		model.addAttribute("olist", teamProjectService.orderList());
		return "admin/ooption";
	}

	@GetMapping("/poption")
	public String pOption(ModelMap model) {
		List<Product> pList = teamProjectService.getProductList();
		model.addAttribute("plist", pList);
		return "admin/poption";
	}

	// 관리자페이지_상품삭제
	@GetMapping("/poption/del/{pcode}")
	public String pDel(@PathVariable(name = "pcode") int pcode) {
		teamProjectService.delProduct(pcode);
		return "redirect:/poption";
	}

	@RequestMapping(value = "/addp", method = RequestMethod.POST)
	public String addProduct(@ModelAttribute Product product, MultipartFile file, HttpServletRequest request,
			HttpSession session) throws IOException, Exception {

//		String uploadPath = session.getServletContext().getRealPath("/")+"/resources/images/";
//		String uploadPath = "D:\\2021\\workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\TeamProject\\resources\\images\\";
		String uploadPath = "C:\\Users\\jun\\git\\_multi\\TeamProject\\resource\\images\\";
		
		String imgUploadPath = uploadPath + File.separator + "imgUpload";
		String ymdPath = UploadFileUtils.calcPath(imgUploadPath);
		String fileName = null;

		if (file.getOriginalFilename() != null && file.getOriginalFilename() != "") {
			fileName = UploadFileUtils.fileUpload(imgUploadPath, file.getOriginalFilename(), file.getBytes(), ymdPath);
		} else {
			fileName = uploadPath + File.separator + "images" + File.separator + "none.png";
		}

		product.setPimg(
				File.separator + "imgUpload" + ymdPath + File.separator + "s" + File.separator + "s_" + fileName);
		;

		int code = Integer.parseInt(request.getParameter("code"));
		product.setPcode(code);

		teamProjectService.addProduct(product);
		return "redirect:/poption";
	}

	// 관리자페이지_고객관리(리스트)
	@GetMapping("/moption")
	public String mOption(ModelMap model) {
		List<Member> mList = teamProjectService.getMemberList();
		model.addAttribute("mlist", mList);
		return "admin/moption";
	}

	// 관리자페이지_고객삭제
	@GetMapping("/moption/del/{id}")
	public String mDel(@PathVariable(name = "id") String id) {
		teamProjectService.delMember(id);
		return "redirect:/moption";
	}

	@GetMapping("/cartList/del/{cpcode}")
	public String cDel(@PathVariable(name = "cpcode") int cpcode) {
		teamProjectService.delCart(cpcode);
		return "redirect:/cartList";
	}

	@GetMapping("/memberInfo")
	public String memberInfo(HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String id = member.getId();
		model.addAttribute("member", teamProjectService.memberInfo(id));
		return "member/memberInfo";
	}

	@GetMapping("/memberUpdate")
	public String memberUpdate(HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String id = member.getId();
		model.addAttribute("member", teamProjectService.memberInfo(id));
		return "member/memberUpdate";
	}

	@RequestMapping(value = "/mUpdate", method = RequestMethod.POST)
	public String memberUpdate1(HttpServletRequest request, HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String id = member.getId();
		model.addAttribute("member", teamProjectService.memberInfo(id));

		String password = request.getParameter("password1");
		String phone = request.getParameter("phone");
		String address = request.getParameter("address");

		member.setPassword(password);
		member.setPhone(phone);
		member.setAddress(address);
		teamProjectService.memberUpdate(member);
		return "redirect:/memberInfo";
	}

	@GetMapping("/memberDelete")
	public String memberDelete(HttpSession session, ModelMap model) {
		Member member = (Member) session.getAttribute("member");
		String id = member.getId();
		model.addAttribute("member", teamProjectService.memberInfo(id));
		return "member/memberDelete";
	}

	@GetMapping("/test")
	public String test() {
		return "test";
	}
	//////

	@GetMapping("/mDel/{id}")
	public String memberDelete1(@PathVariable(name = "id") String id, HttpSession session) {
		teamProjectService.delMember(id);
		session.invalidate();
		return "redirect:/main";
	}
}
