CREATE TABLE member(
id varchar2(15) primary key,
password varchar2(15) not null,
name varchar2(12) not null,
birthday date not null,
phone varchar2(15) not null,
gender varchar2(6) not null,
email varchar2(25) not null,
address varchar2(30) not null
);

insert into member values('testid', 'testpw', '�׽�Ʈ', sysdate, '01012345678', '����', 'test@naver.com', '����Ư����');
insert into member values('testid2', 'testpw', '�׽�Ʈ2', sysdate, '01023456789', '����', 'test2@naver.com', '�λ걤����');

create user multi identified by "1234";

grant connect, dba, resource to multi;

CREATE TABLE product(
pname varchar2(30) not null,
pcode number(10) primary key,
pbrand varchar2(15),
pprice number(10) not null,
pdate date);
--,
--psprice number(10),
--pdiscount number(3))
--pimg blob);

create sequence product_seq;
insert into product values('배변봉투',20001, 't_brand1', 20000, sysdate);
purchase
주문번호, 고객아이디, 상품번호, 주문날짜, 

CREATE sequence order_seq
start with 1
increment by 1;

insert into porder values(1, 'testid2', 10001, sysdate, 3);

CREATE sequence onum_seq
start with 1
increment by 1;

cart
고객아이디, 상품코드, 수량

CREATE TABLE cart(
ccode number(10) primary key,
cid varchar2(15),
cpcode number(10),
cquantity number(5) not null,
cprice number(10) not null,
cdate date,
constraint fk_cid foreign key(cid) references member(id),
constraint fk_cpcode foreign key(cpcode) references product(pcode));

CREATE sequence cart_seq
start with 1
increment by 1;

alter table product modify(pbrand varchar2(30));


create table porder(
ocode number(10) primary key,
ocid varchar2(15),
orec varchar2(50),
oaddress varchar2(30),
ophone varchar2(15),
oprice number(10),
odate date,
constraint fk_member foreign key(ocid) references member(id));

create table porder_detail(
odcode number primary key,
oid number(10),
opcode number(10),
oquantity number(5),
constraint fk_oid foreign key(oid) references porder(ocode));

create sequence porder_detail_seq;

create sequence product_seq;

alter table product add (content varchar(200));
alter table product add (pimg varchar(200));
